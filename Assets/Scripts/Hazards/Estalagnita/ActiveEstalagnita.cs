﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveEstalagnita : MonoBehaviour
{
    public GameObject EstalagnitaActive ;
   
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == GameManager.player_id && EstalagnitaActive==true)
        {
            EstalagnitaActive.GetComponent<Estalagnita>().enabled = true;
        }
    }
}
