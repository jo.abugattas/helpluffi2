﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Estalagnita : MonoBehaviour
{
    public float DamageEstalagnita = 10;

    public Transform target;
    public float speed;
    void Update()
    {
        float step = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, target.position, step);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject == GameManager.player_id)
        {
            other.GetComponent<CControl>().ActualEnergy -= DamageEstalagnita;
            Destroy(gameObject);
        }
    }
}
