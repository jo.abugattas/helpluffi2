﻿using UnityEngine;

public class CamCol : MonoBehaviour
{
    public Transform referenceTransform;
    public float collisionOffset = 0.2f; //To prevent Camera from clipping through Objects

    Vector3 defaultPos;
    Vector3 directionNormalized;
    Transform parentTransform;
    float defaultDistance;

    public Transform Target, Player;
    public Transform Obstruction;
    public GameObject ff;
    // Start is called before the first frame update
    void Start()
    {

        Obstruction = Target;

        defaultPos = transform.localPosition;
        directionNormalized = defaultPos.normalized;
        parentTransform = transform.parent;
        defaultDistance = Vector3.Distance(defaultPos, Vector3.zero);
    }

    // FixedUpdate for physics calculations
    void FixedUpdate()
    {
        int layerMask = 1 << 9;
        CamControl();

        Vector3 currentPos = defaultPos;
        Vector3 vector3 = transform.TransformDirection(Vector3.forward);
        RaycastHit hit;
        Vector3 dirTmp = parentTransform.TransformPoint(defaultPos) - referenceTransform.position;
        if (Physics.SphereCast(referenceTransform.position, collisionOffset, dirTmp, out hit, defaultDistance))
        {
            currentPos = (directionNormalized * (hit.distance - collisionOffset));
        }

        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, 10, layerMask) && GetComponentInParent<CControl>().currentState == CControl.States.hand)
        {
            hit.collider.gameObject.GetComponent<PickUp>().OnMouseDown();
            
        }

        transform.localPosition = Vector3.Lerp(transform.localPosition, currentPos, Time.deltaTime * 15f);

        //FindFruit();

    }

    void CamControl()
    {
        transform.LookAt(Target);
    }

    /*private void FindFruit() {
        //RaycastHit HitFruit;
        
        
    }*/
}