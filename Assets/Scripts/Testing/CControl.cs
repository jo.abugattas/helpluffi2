﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

[RequireComponent(typeof(CharacterController))]

public class CControl : MonoBehaviour
{
    public int Parts = 0;
    public int NumBullets = 0;
    public float TotalEnergy = 100; // Energia Total.
    public float ActualEnergy; // Energia Actual
    public EnergyBar NergyBar; // Barra de Energia
    public  float MinusE = 1.5f; // Cantidad de Energia que va perdiendo
    public bool IsDead = false;
    public Text PartsText;
    public Text EnergyText;
    public Text NumBulletsText;
    private GameObject gun;
    public States currentState;

    public enum States{
        weapon,
        hand,
    }

    public float speed = 7.5f;
    public float jumpSpeed = 8.0f;
    public float gravity = 20.0f;
    public Transform playerCameraParent;
    public float lookSpeed = 2.0f;
    public float lookXLimit = 40.0f;

    CharacterController characterController;
    Vector3 moveDirection = Vector3.zero;
    Vector2 rotation = Vector2.zero;

    [HideInInspector]
    public bool canMove = true;

    // Fade
    public UnityEngine.UI.Image fade_black;
    float ColorFade_black;

    //UI
    public GameObject Panel_win;
    public GameObject Panel_over;
    public GameObject HUD;



    void Start()
    {
        characterController = GetComponent<CharacterController>();
        rotation.y = transform.eulerAngles.y;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        NergyBar.SetMaxEnergy(TotalEnergy);
        ActualEnergy = TotalEnergy; // Valor de Energia para Energia Actual
        gun = GetComponentInChildren<Gun>().gameObject;

        //fade inicial
        fade_black.color = new Color(0, 0, 1); // fade_black
        ColorFade_black = 0; // fade_transparente
    }

    void Update()
    {
        if (characterController.isGrounded)
        {
            // We are grounded, so recalculate move direction based on axes
            Vector3 forward = transform.TransformDirection(Vector3.forward);
            Vector3 right = transform.TransformDirection(Vector3.right);
            float curSpeedX = canMove ? speed * Input.GetAxis("Vertical") : 0;
            float curSpeedY = canMove ? speed * Input.GetAxis("Horizontal") : 0;
            moveDirection = (forward * curSpeedX) + (right * curSpeedY);

            if (Input.GetButton("Jump") && canMove)
            {
                moveDirection.y = jumpSpeed;
            }
        }

        // Apply gravity. Gravity is multiplied by deltaTime twice (once here, and once below
        // when the moveDirection is multiplied by deltaTime). This is because gravity should be applied
        // as an acceleration (ms^-2)
        moveDirection.y -= gravity * Time.deltaTime;

        // movimiento del controller
        characterController.Move(moveDirection * Time.deltaTime);

        // Rotacion camara y player
        if (canMove)
        {
            rotation.y += Input.GetAxis("Mouse X") * lookSpeed;
            rotation.x += -Input.GetAxis("Mouse Y") * lookSpeed;
            rotation.x = Mathf.Clamp(rotation.x, -lookXLimit, lookXLimit);
            playerCameraParent.localRotation = Quaternion.Euler(rotation.x, 0, 0);
            transform.eulerAngles = new Vector2(0, rotation.y);
        }

        Text();
        LosesEnergy();
        EnergyCheck();
        ActualEnergy = Mathf.Clamp(ActualEnergy,0,TotalEnergy);

        if (Input.GetKeyDown(KeyCode.E)) {
            currentState = currentState == States.hand ? States.weapon : States.hand;
            print(currentState);
        }

        switch (currentState)
        {
            case States.weapon:
                gun.SetActive(true);
                break;
            case States.hand:
                gun.SetActive(false);
                break;
            default:
                break;
        }

        if (Input.GetKeyDown(KeyCode.P)) ColorFade_black = 1; // fade Out
        //if (Input.GetKeyDown(KeyCode.O)) ColorFade_black = 0; // fade in
        


    }
    private void FixedUpdate()
    {
        float valorAlfa = Mathf.Lerp(fade_black.color.a, ColorFade_black, 0.03f);
        fade_black.color = new Color(0, 0, 0, valorAlfa);

        //reiniciar escena al finalizar Fade
        if (valorAlfa > 0.9f && ColorFade_black == 1)
        {
            SceneManager.LoadScene("Level1");
        }


    }

    void LosesEnergy() {
        ActualEnergy -= MinusE * Time.deltaTime;
        NergyBar.SetEnergy(ActualEnergy); 
    } // Pierde Energia

    

    void EnergyCheck() {

        if (ActualEnergy <= 0)
        {
            Panel_over.SetActive(true);
            HUD.SetActive(false);
            ColorFade_black = 0.6f; // fade Out Negro
            Debug.Log("DEAD");
        }
    }

    //void Winner()
    //{
    //    if (piezasrecolectadas == Total)
    //    {
    //        Panel_win.SetActive(true);
    //    }
    //}

    void Text() {
        PartsText.text = "Parts" + Parts;
        //EnergyText.text = "Energy: " + ActualEnergy;
        NumBulletsText.text = NumBullets + " / 5";
    }

    void Weapon()
    {

    }
}