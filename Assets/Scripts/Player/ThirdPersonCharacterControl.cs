﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ThirdPersonCharacterControl : MonoBehaviour {

    public int Parts = 0;
    public int NumBullets = 0;

    public float Speed;
    public float TotalEnergy = 100; // Energia total. Puede modificarse en Inspector
    public float ActualEnergy; // Energia Actual
    private float JumpSpeed = 5; // Poder de salto
    public EnergyBar Nergybar; // Barra de Energia
    private bool OnGround = true;
    public bool IsDead = false;

    private const float MinusE = 1.5f; // Cantidad de Energia que va perdiendo

    public Text PartsText;
    public Text EnergyText;
    private Rigidbody Rgbd;

    public TMPro.TextMeshProUGUI NumBulletsText;

   
    void Start() {
        Nergybar.SetMaxEnergy(TotalEnergy);
        ActualEnergy = TotalEnergy; // valor de Energia para a Energia Actual
        Rgbd = GetComponent<Rigidbody>();
    }

	void Update () {
        LosesEnergy();
        EnergyCheck();
        if (IsDead == false) { PlayerJump(); PlayerMovement(); } else { };
        Text();
        ActualEnergy = Mathf.Clamp(ActualEnergy,0,TotalEnergy);
    }

    // Movimiento
    void PlayerMovement() { 
        float hor = Input.GetAxis("Horizontal");
        float ver = Input.GetAxis("Vertical");
        Vector3 playerMovement = new Vector3(hor, 0f, ver) * Speed * Time.deltaTime;
        transform.Translate(playerMovement, Space.Self);
    }

    // Salto
    void PlayerJump() {
        if (Input.GetKeyDown("space") && OnGround) {
            Rgbd.AddForce(Vector3.up * JumpSpeed, ForceMode.Impulse);
            OnGround = false;
        }
    }

    void OnCollisionEnter(Collision collision) { OnGround = true; }
    void EnergyCheck() {
        if (ActualEnergy <= 0) { Debug.Log("DEAD");
            EnergyText.text = "NO ENERGY ";
        };
    } //Cambia el mensaje si no tiene Energia  y destruye al personaje

    void LosesEnergy() {
        ActualEnergy -= MinusE * Time.deltaTime;
        Nergybar.SetEnergy(ActualEnergy);
    }  //Pierde energia

    void Text() {
        PartsText.text = "Parts: " + Parts;
        EnergyText.text = "Energy: " + ActualEnergy;
        NumBulletsText.text = "Bullets: " + NumBullets; }
}