﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnergyBar : MonoBehaviour
{
    public Slider slider;
    public void SetMaxEnergy(float Energy)
    {
        slider.maxValue = Energy;
        slider.value = Energy;
    }
    public void SetEnergy(float Energy) { slider.value = Energy; }

  
}
