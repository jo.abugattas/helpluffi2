﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Battery : MonoBehaviour
{
    public float BatPlus = 20;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player") {
            other.GetComponent<CControl>().ActualEnergy += BatPlus;
            Destroy(gameObject);
        }
    }
}
