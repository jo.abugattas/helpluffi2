﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GameObject Death,Win;
    public ThirdPersonCharacterControl PlayerStats;
    public float EnergyA;
    public int Parts;
    public bool PlayerDead;

    private static GameObject player;
    public static GameObject player_id {
        get {
            if (player == null) {
                player = GameObject.FindGameObjectWithTag("Player");
            }
            return player;
        }
        set {
            player_id = player;
        }
    }
    public static GameObject picketFruit;
    public static int parts;
    public static int maxParts;
    public List<GameObject> rocks = new List<GameObject>();

    // Start is called before the first frame update
    void Start(){
        maxParts = 10;
    }
    private void FixedUpdate() {
        EnergyLeft();
        WinC();
        NexLevel();
    }
    void EnergyLeft()
    {
        EnergyA = player_id.GetComponent<CControl>().ActualEnergy;
        if (EnergyA <= 0)
        {
            PlayerDead = player_id.GetComponent<CControl>().IsDead = true;
            Death.SetActive(true);
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
    }
    void WinC()
    {
        Parts = player_id.GetComponent<CControl>().Parts;
        if (Parts >= 5)
        {
            PlayerDead = player_id.GetComponent<CControl>().IsDead = true;
            Win.SetActive(true);
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
    }
    private void NexLevel() {
         if(parts != maxParts ) {
            return;
        } else {
            foreach(GameObject gameObject in rocks ) {
                Destroy(gameObject);
            }
        }
    }
}
