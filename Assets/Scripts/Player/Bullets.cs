﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullets : MonoBehaviour
{
    public int numBulletsTotal=5;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == GameManager.player_id)
        {
            if (other.GetComponent<CControl>().NumBullets==0)
            {
                other.GetComponent<CControl>().NumBullets +=5;
                Destroy(gameObject);
            }
            else
            {
                other.GetComponent<CControl>().NumBullets += (numBulletsTotal- other.GetComponent<CControl>().NumBullets);
                Destroy(gameObject);
            }
            
        }
        
    }
}
