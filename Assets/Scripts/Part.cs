﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Part : MonoBehaviour {
    private void OnTriggerEnter (Collider other) {
        if (other.gameObject == GameManager.player_id) {
            other.GetComponent<CControl>().Parts++;
            Destroy(gameObject);
        }
    }
}
