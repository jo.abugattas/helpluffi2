﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAttack : MonoBehaviour
{
    public float DamageBoss = 25;
    public EnemyScript EnsBoss;
    public float Timer = 5f;


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == GameManager.player_id)
        {
            EnsBoss.GetComponent<EnemyScript>().IsAttacking = true;
            while (EnsBoss == true && Timer <=0)
            {
                other.GetComponent<CControl>().ActualEnergy -= DamageBoss;
                Timer = 5f;
                Debug.Log("Ataque Jefe");
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject == GameManager.player_id)
        {
            EnsBoss.GetComponent<EnemyScript>().IsAttacking = false;
        }
    }
}
