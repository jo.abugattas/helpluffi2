﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeScript : MonoBehaviour
{
    public GameObject Enemy;
    public GameObject Cube;

    void Start()
    {
        StartCoroutine(TransformToEnemy());
    }
    IEnumerator TransformToEnemy()
    {
        yield return new WaitForSeconds(5);
        Convert();
    }
    void Convert()
    {
        Instantiate(Enemy, new Vector3(Cube.transform.position.x, Cube.transform.position.y, Cube.transform.position.z), Quaternion.identity);
        Destroy(Cube);
    }
}
