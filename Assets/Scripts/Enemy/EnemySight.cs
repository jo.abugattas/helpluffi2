﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySight : MonoBehaviour
{
    public GameObject FoV;
    public EnemyScript EnemyV;
    public float LCont = 0;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == GameManager.player_id)
        {
            EnemyV.GetComponent<EnemyScript>().Detected = true;
            EnemyV.GetComponent<EnemyScript>().Player = other.transform;
        }
        else { }      
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject == GameManager.player_id)
        {
                LCont = 0; LosingPlayer();
        }
    }
    void LosingPlayer()
    {
            EnemyV.GetComponent<EnemyScript>().Detected = false;
            EnemyV.GetComponent<EnemyScript>().Player = null;
    }  
}
