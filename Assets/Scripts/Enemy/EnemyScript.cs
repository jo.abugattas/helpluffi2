﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyScript : MonoBehaviour
{
    public GameObject Cube, Enemy;
    public bool Convert = false;
    public bool Detected = false;
    public Transform Player;
    float MoveSpeed = 10f, RotSpeed = 3f;
    public bool IsAttacking = false;

    void Start() {}
    void Update()
    {
        
        if (Convert) { ConvertToCube(); };
        Detection();
    }
    void ConvertToCube()
    {
        Instantiate(Cube, new Vector3(Enemy.transform.position.x, Enemy.transform.position.y, Enemy.transform.position.z), Quaternion.identity);
        Destroy(Enemy);
    }
    void Detection()
    {
        if (Detected && IsAttacking == false) { Follow(); Debug.Log("detectado"); } else if(IsAttacking) { Debug.Log("Atacando"); }
    }
    void Follow()
    {
       Vector3 my_forward = transform.forward;
       my_forward.y = 0;
       transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(Player.position - transform.position), RotSpeed * Time.deltaTime);
       transform.position += my_forward.normalized * MoveSpeed * Time.deltaTime; 
    }

}
