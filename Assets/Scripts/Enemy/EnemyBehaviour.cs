﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditorInternal;
using UnityEngine;
using UnityEngine.Video;

public class EnemyBehaviour : MonoBehaviour
{

    [Header("Path")]
    public List<Transform> points = new List<Transform>();
    public Transform start;
    public Transform end;
    public bool starpath;
    public float dist;

    [Header("VisualCamp")]
    [Range(0f, 360f)] public float visualAngle = 30f;
    public float vissionDistance = 10f;
    public bool playerInRange;
    public bool followingPlayer;

    [Header("FrutAreadDeteccion")]
    public float radio;
    public bool followingFruit;

    [Header("Stats")]
    public float speed;
    float timer = 2;
    public float attackRange;
    public float damage;
    public bool hitPlayer;
    float nexHit;
    public enum states{idle,path,follow,attack,followFruit,}
    public states currentState;
    private Animator anim;

    int i;
    Rigidbody rb;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        starpath = true;
        followingPlayer = false;
        followingFruit = false;
        hitPlayer = false;
        i = 0;
        currentState = states.path;
        if (starpath && points.Count != 0){
            start = points[i];
            end = points[i + 1];
            i = i + 1;
            starpath = false;
            LookAt(end.position);
        }
    }
    void Update(){
        switch (currentState){
            case states.idle:
                //anim.SetBool("Idle", true);
                speed = 0;
                if (timer != 0){
                    timer -= 1 * Time.deltaTime;
                    if (timer <= 0)
                        timer = 0;
                }
            break;
            case states.path:
            Route();
            if( points.Count == 0 ) {
                return;
            } else {
                speed = 5;
                dist = Vector3.Distance(start.position, end.position);
                float enemyDist = Vector3.Distance(transform.position, end.position);
                //anim.SetTrigger("Run");
                if( dist != 0 ) {
                    rb.velocity = transform.forward * speed;
                    dist = enemyDist;
                }
            }
            break;
            case states.follow:
                speed = 5;  
                FolowPlayer();
            break;
            case states.attack:
            speed = 0;
            float playerDist = Vector3.Distance(transform.position, GameManager.player_id.transform.position);
            float energyReduce = ( damage * GameManager.player_id.GetComponent<CControl>().ActualEnergy ) / 100;
            if( playerInRange ) {
                if(playerDist < attackRange && !hitPlayer) {
                    GameManager.player_id.GetComponent<CControl>().ActualEnergy -= energyReduce;
                    hitPlayer = true;
                    nexHit = 3f;
                } else if(playerDist > attackRange){
                    PlayerInRange();
                    speed = 5;
                } 
            }else if(!playerInRange) {
                GetBackToRoud();
            }
            if(hitPlayer) {
                if( nexHit != 0 ) {
                    nexHit -= 1 * Time.deltaTime;
                    if( nexHit <= 0 ) {
                        nexHit = 0;
                    }
                } else if( nexHit == 0 ) {
                    hitPlayer = false;
                }
            }   

            break;
            case states.followFruit:
            FollorFruit();
            break;
            default:
                break;
        }
        if (timer == 0 && currentState == states.idle){
            currentState = states.path;
        }
        if( playerInRange && !followingPlayer ) {
            currentState = states.follow;
            followingPlayer = true;
        }else if(!playerInRange && followingPlayer ) {
            GetBackToRoud();
        }

        PlayerInRange();
        FruitInRange();
    }
    void Route(){
        if( points.Count == 0 )
            return;
        if (dist <= 1){
            currentState = states.idle;
            if(i == points.Count - 1 &&  timer == 0){
                i = 0;
                start = end;
                end = points[i];
                dist = Vector3.Distance(start.position, end.position);
                timer = 2;
                LookAt(end.position);
            }
            else if(i != points.Count - 1 && timer == 0){
                start = end;
                end = points[i + 1];
                i = i + 1;
                dist = Vector3.Distance(start.position, end.position);
                timer = 2;
                LookAt(end.position);
            }
        }
    }
    private void GetBackToRoud() {
        LookAt(end.position);

        rb.velocity = transform.forward * speed;

        float dist = Vector3.Distance(transform.position, end.position);
        if(dist <= 1 ) {
            currentState = states.path;
            starpath = true;
            followingPlayer = false;
        }
    }
    private void PlayerInRange() {
        playerInRange = false;

        Vector3 playerVector = GameManager.player_id.transform.position - transform.position;
        if(Vector3.Angle(playerVector.normalized,transform.forward) < visualAngle/2) {
            if(playerVector.magnitude < vissionDistance ) {
                playerInRange = true;
            }
        }
    }
    private void FruitInRange() {
        float distanceFruit;
        if(GameManager.picketFruit != null ) {
            distanceFruit = Vector3.Distance(transform.position,
                              GameManager.picketFruit.transform.position);
            if( distanceFruit < radio ) {
                followingFruit = true;
            }
        }
        if( followingFruit ) {
            currentState = states.followFruit;
        }
    }
    private void FollorFruit() {
        LookAt(GameManager.picketFruit.transform.position);
        rb.velocity = transform.forward * speed;
        float dist = Vector3.Distance(transform.position, GameManager.picketFruit.transform.position);
        if( dist < 1) {
            GetBackToRoud();
        }
    }
    void LookAt(Vector3 look){
        look.y = transform.position.y;
        transform.LookAt(look);
    }   
    public void FolowPlayer(){
        LookAt(GameManager.player_id.transform.position);
        starpath = false;
        rb.velocity = transform.forward * speed;
        float dist = Vector3.Distance(transform.position, GameManager.player_id.transform.position);
        if(dist <= attackRange ) {
            currentState = states.attack;
        }

    }
    private void OnDrawGizmos() {
        if( visualAngle == 0 )
            return;

        float half_angle = visualAngle / 2;

        Vector3 p1, p2;
        p1 = PointForAngle(half_angle,vissionDistance);
        p2 = PointForAngle(-half_angle,vissionDistance);

        Gizmos.color = playerInRange ? Color.green : Color.red;

        Gizmos.DrawLine(transform.position , transform.position + p1);
        Gizmos.DrawLine(transform.position , transform.position + p2);

        Gizmos.color = Color.cyan;

        Gizmos.DrawWireSphere(transform.position, radio);
    }
    Vector3 PointForAngle(float angle , float distance) {
        return transform.TransformDirection(new Vector3(Mathf.Sin(angle * Mathf.Deg2Rad), 0, Mathf.Cos(angle * Mathf.Deg2Rad))) * distance;
    }
}
