﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    public float DamageEnemy = 10; //daño del enemigo
    public EnemyScript EnS;
    float timer = 3f;
    
    void OnTriggerStay(Collider other)
    {
        if (other.gameObject == GameManager.player_id)
        {
            EnS.GetComponent<EnemyScript>().IsAttacking = true;
            timer -= Time.deltaTime;
            if (timer < 0)
            {
                if (other.gameObject == GameManager.player_id)
                {
                    other.GetComponent<CControl>().ActualEnergy -= DamageEnemy;
                    timer = 3f;
                    Debug.Log("Golpe");
                }
                return;           
            }
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject == GameManager.player_id)
        {
            EnS.GetComponent<EnemyScript>().IsAttacking = false;
        }
    }
    
}
