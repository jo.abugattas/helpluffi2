﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
    //cambiar esto por un destroy ya que los enemigos no tiene vida
    public float health = 100f;

    public void TakeDamage (float amount)
    {
        //cambiar esto por un destroy ya que los enemigos no tiene vida
        health -= amount;
        if(health <= 0f)
        {
            Die();
        }

        
    }
    void Die()
    {
        Destroy(gameObject);
    }
}
