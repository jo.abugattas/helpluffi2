﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    public float damage = 100f;
    public float range = 100f;
    public int bullets;
    public GameObject Hit;

    public Camera CameraController;
    void Update()
    {
        if (Input.GetButtonDown("Fire1") )
        {
            bullets = GetComponentInParent<CControl>().NumBullets;
            if (bullets > 0)
            {
                bullets = GetComponentInParent<CControl>().NumBullets -= 1;
                Shoot();
            }
           
        }
    }

    void Shoot()
    {
        //EnemyScript ES = GetComponent<EnemyScript>();
        int enemyLayer = 1 << 10;
        RaycastHit hit; 
        if(Physics.Raycast(CameraController.transform.position, CameraController.transform.forward, out hit, range, enemyLayer))
        {
            Debug.Log(hit.transform.name);
            Target target = hit.transform.GetComponent<Target>();
            Hit = hit.transform.gameObject;
            if (Hit.tag == "Enemy")
            {
                EnemyScript ES = Hit.GetComponent<EnemyScript>();
                ES.Convert = true;
                Hit = null;
            }
            //else if(target != null)
            //{
            //    target.TakeDamage(damage);
            //}
        }

    }
}
