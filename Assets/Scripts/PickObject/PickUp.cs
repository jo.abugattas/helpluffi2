﻿using System.Collections;
using System.Collections.Generic;
//using UnityEditor.Experimental.UIElements;
using UnityEngine;

public class PickUp : MonoBehaviour
{
    float throwForce = 1200;
    float distance;
    Vector3 objectPos;
    public bool canHold = true;
    public bool isHolding = false;
    public GameObject item;
    public GameObject tempParent;

    void Update(){
        distance = Vector3.Distance(item.transform.position, GameManager.player_id.transform.position);
        if (distance >= 1.5f){isHolding = false;}
        if (isHolding == true){
            item.GetComponent<Rigidbody>().velocity = Vector3.zero;
            item.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
            item.transform.SetParent(GameManager.player_id.transform);
            if (Input.GetMouseButtonDown(1)){
                item.GetComponent<Rigidbody>().AddForce(GameManager.player_id.transform.forward * throwForce);
                GameManager.picketFruit = item;
                isHolding = false;
            }
            print(1);
        }
        else{
            objectPos = item.transform.position;
            item.transform.SetParent(null);
            item.GetComponent<Rigidbody>().useGravity = true;
            item.transform.position = objectPos;
        }
       
    }

    public void OnMouseDown(){
        isHolding = true;
        item.GetComponent<Rigidbody>().useGravity = false;
        item.GetComponent<Rigidbody>().detectCollisions = true;
        if (distance <= 1.5f){
            isHolding = true;
            item.GetComponent<Rigidbody>().useGravity = false;
            item.GetComponent<Rigidbody>().detectCollisions = true;
        }
        print(2);
    }

    void OnMouseUp(){
        isHolding = false;
    }


}
