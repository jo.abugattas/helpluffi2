﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arena : MonoBehaviour
{
    public int damage_arena = 10;
    public float velrest = 4;
    float velAct;
    float MinusE_Star;
    public float dmg;
    public bool playerStay;
 
    private void Start() {
        velAct = GameManager.player_id.GetComponent<CControl>().speed;
        MinusE_Star = GameManager.player_id.GetComponent<CControl>().MinusE;
        playerStay = false;
    }
    private void TookEnergy() {
        if(playerStay) {
            GameManager.player_id.GetComponent<CControl>().MinusE = ( ( 100 + dmg ) * MinusE_Star ) / 100;
            playerStay = false;
        }
    }
    void OnTriggerStay(Collider other){
      if(other.gameObject == GameManager.player_id ) {
            GameManager.player_id.GetComponent<CControl>().speed = velrest;
            playerStay = true;
            TookEnergy();
        }
    }
    private void OnTriggerExit( Collider other ) {
        if(other.gameObject == GameManager.player_id ) {
            GameManager.player_id.GetComponent<CControl>().speed = velAct;
            GameManager.player_id.GetComponent<CControl>().MinusE = MinusE_Star;
        }
    }
}
